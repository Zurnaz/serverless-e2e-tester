# serverless e2e tester

Status: Failed

Experimental attempt to have a browser based tester in Lambda.

Note: Code is functional to an extent but not ideal, only really to illustrate as an example.

## requirements

```bash
nodejs 10+
```

## Usage

Install dependencies. There is a custom install to ensure cypress cache gets loaded to the correct place.

```bash
yarn installv2
```

Open cypress and visually see what the code is doing

```bash
yarn dev
```

Run the code in headless mode

```bash
yarn test
```

Lint the code and auto correct

```bash
yarn lint
```

Jest unit tests are separate

```bash
yarn unit
```