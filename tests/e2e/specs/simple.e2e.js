describe('simple test', () => {
  it('successful login works redirects to the home page and logging out works', () => {
    cy.visit('/blog')
    cy.contains('vuejs').click()
    cy.location('pathname').should('equal', '/tags/vuejs/')
    cy.contains('a', 'gitlab').click()
    cy.wait(3000) // 5000 = 5 seconds
  })
})
