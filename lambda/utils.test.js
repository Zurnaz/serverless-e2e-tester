const assert = require('chai').assert
const generateJsonResponse = require('./utils').generateJsonResponse
it('generate json response string', (done) => {
  const response = generateJsonResponse(200, 'Great success')
  const expectedBody = JSON.stringify({ message: 'Great success' })
  assert.equal(expectedBody, response.body)
  assert.equal(200, response.statusCode)
  done()
})

it('generate response dynamodb with no items', (done) => {
  const input = { Items: [], Count: 0, ScannedCount: 0 }
  const response = generateJsonResponse(200, input)
  const expectedBody = JSON.stringify(input)
  assert.equal(response.body, expectedBody)
  assert.equal(200, response.statusCode)
  done()
})
