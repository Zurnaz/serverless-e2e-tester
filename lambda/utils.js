module.exports.generateJsonResponse = (statusCode, message) => {
    if (typeof statusCode !== 'number') {
      throw new Error('generateJsonResponse:: Invalid status code')
    }
    const prebody = typeof message === 'string' ? { message } : message
    const body = JSON.stringify(prebody)
    return {
      statusCode,
      headers: { 'Content-Type': 'application/json' },
      body: '' + body,
    }
  }