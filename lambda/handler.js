const generatejson = require('./utils').generateJsonResponse

// Exporting the dependencies like this allows you to mock them in unit tests
module.exports.AWS = require('aws-sdk')
module.exports.sns = new this.AWS.SNS();
module.exports.cypress = require('cypress')
// End of mockable dependencies

// Injected parameters
const snsTopicArn = process.env.SNS_TOPIC_ARN
// End of Injected parameters

const TEST_LIST = {'simple': true}

module.exports.loadtest = async (event, context) => {
  const testType = event.pathParameters.id
  if (!TEST_LIST[testType]){
    return generatejson(200, `Test ${testType} does not exist`)
  }
  const params = {
    Message: `Tests for ${testType}`,
    MessageAttributes: {
      action: {
          DataType: 'String',
          StringValue: testType
        }
    },
    TopicArn: snsTopicArn,
  }
  return this.sns.publish(params).promise().then(()=>{
    return generatejson(200, `Test ${testType} successfully created`)
  });
  
}

module.exports.simple = async (event, context) => {
  return this.cypress.run({
    spec: 'tests/e2e/specs/simple.e2e.js',
    config: {
      "pluginsFile": "tests/e2e/plugins/index.js",
      "baseUrl": "https://www.bogdandrema.com",
      "video": false,
    },
  })
  .then((results) => {
    // console.log(results)
    const response = results.message ? results.message : 'simple testing finished running'
    console.log(response)
    return response
  })
  .catch((err) => {
    console.error(err)
    return 'simple testing failed to run'
  })
  
}
